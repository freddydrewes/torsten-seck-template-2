/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code of
 * the TYPO3 source package.
 *
 * The TYPO3 project - inspiring people to share!
 */

$(document).ready(function () {

	var sections = $('.section');
	var countSections = sections.length;
	var sectionNav = $('#section-nav');


	sections.each(function () {
		sectionNav.append('<a class="dot" href="#'+$(this).attr('id')+'"></a>');
		$(this).find('img').addClass('TEST');
	});

	var button = $('a.btn');
	button.click(function (e) {
		if($(this)[0].href.indexOf("#") != -1) {
			e.preventDefault();
			console.log('check');
			var newUrl = $(this)[0].hash.replace('c', 'section-');
			console.log(newUrl);
			$('html, body').animate({
				scrollTop: $(newUrl).offset().top
			}, 800);
		}
	});

	$('.main-menu > li').click(function (e) {
		var link = $(this).find('a');
		if($(this).hasClass('subsite-menu')) {
		} else {
			if(link.hasClass('hash-menu-item')) {
				e.preventDefault();
				var hash = link[0].hash;
				console.log(hash);
				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 800);
			}
		}

	});

	$('.dot').click(function (e) {
		e.preventDefault();
		var sectionId = $(this).attr('href');

		var dots = $('#section-nav .dot');
		dots.each(function () {
			$(this).removeClass('dot-active');
		});

		$(this).addClass('dot-active');


		$('html, body').animate({
			scrollTop: $(sectionId).offset().top
		}, 800);
	});

	$('.jarallax').jarallax({
		type: 'scroll',
		// speed: 0.1,
	});

	$('#rwd-toggle').click(function () {
		$('.main-menu').toggleClass('show-menu');
	});

	$('main.subsite').parent().addClass('is-subsite');

	// Sektionen Punkt berücksichtigen die Hintergrundfarbe.
	// Wenn man hinscrollt.

});