<?php

namespace SitePackage\SitePackage\DataProcessing;

/*
 * This file is part of the TYPO3 CMS extension fluid_styled_content.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\ContentObject\Exception\ContentRenderingException;

/**
 * This data processor will calculate the width of a heading
 * based on the included images and is used for the CType "heading"
 */
class HeadingProcessor implements DataProcessorInterface
{

    /**
     * Process data for the CType "heading"
     *
     * @param ContentObjectRenderer $cObj                       The content object renderer, which contains data of the content element
     * @param array                 $contentObjectConfiguration The configuration of Content Object
     * @param array                 $processorConfiguration     The configuration of this processor
     * @param array                 $processedData              Key/value store eof processed data (e.g. to be passed to a Fluid View)
     *
     * @return array the processed data as key/value store
     * @throws ContentRenderingException
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {

        $data               = $this->getOptionsFromFlexFormData($processedData['data']);
        $newArray           = [];

        $newArray['data']   = $data;

        return $newArray;
    }


    /**
     * @param array $row
     *
     * @return array
     */
    protected function getOptionsFromFlexFormData(array $row) {
        $options         = [];
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
        $flexFormAsArray = $flexFormService->convertFlexFormContentToArray($row['pi_flexform']);

        return $flexFormAsArray;
    }
}
