<?php

namespace SitePackage\SitePackage\Hooks;

/*
 * This file is part of the TYPO3 CMS extension fluid_styled_content.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Contains a preview rendering for the page module of CType="heading"
 */
class ButtonPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "heading"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject  Calling parent object
     * @param bool                                   $drawItem      Whether to draw the item using the default functionality
     * @param string                                 $headerContent Header content
     * @param string                                 $itemContent   Item content
     * @param array                                  $row           Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        if ($row['CType'] === 'button') {
            $data = $this->getOptionsFromFlexFormData($row);

            $style = "padding: 10px 20px;";

            if($data['options']['style'] == 'button-filled') {
                $style .= "background-color: " . $data['options']['button_color'] . ";";
                $style .= "color: " . $data['options']['font_color']. ";";
            } else {
                $style .= "border: 2px solid " . $data['options']['button_color'] . ";";
                $style .= "color: " . $data['options']['font_color']. ";";
            }

            if($data['settings']['roundCorners']) {
                $style .= "border-radius: 25px;";
            }

            $itemContent .= "<div class='btn' style='".$style."'>".$data['options']['linktext']."</div>";

            $drawItem    = false;
        }
    }

    protected function getOptionsFromFlexFormData(array $row) {
        $options         = [];
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
        $flexFormAsArray = $flexFormService->convertFlexFormContentToArray($row['pi_flexform']);


        return $flexFormAsArray;

    }

}
