<?php

namespace SitePackage\SitePackage\Hooks;

/*
 * This file is part of the TYPO3 CMS extension fluid_styled_content.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Contains a preview rendering for the page module of CType="heading"
 */
class ParallaxThreeColsPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "heading"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject  Calling parent object
     * @param bool                                   $drawItem      Whether to draw the item using the default functionality
     * @param string                                 $headerContent Header content
     * @param string                                 $itemContent   Item content
     * @param array                                  $row           Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        if ($row['CType'] === 'parallax_three_cols') {
            $data = $this->getOptionsFromFlexFormData($row);


            if (!empty($data['image'])) {
                $tempImageIds       = explode(',', $data['image']);
                $newArray['images'] = $tempImageIds;
            }

            if (!empty($data['image'])) {
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('sys_file_reference')->createQueryBuilder();
                $statement    = $queryBuilder
                    ->select('*')
                    ->from('sys_file_reference')
                    ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($data['image']))
                    )
                    ->execute();

                $fileUid = $statement->fetch();
                if(!empty($fileUid)) {
                    $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
                    $file = $resourceFactory->getFileObject($fileUid['uid_local']);
                }
            }

            $itemContent .= "<strong>Parallax</strong><br>";
            if(!empty($file)) {
                $itemContent .= "<img src='/fileadmin/".$file->getIdentifier()."' style='max-width: 200px; width:100%;' /><br><br>";
            }
            $itemContent .= $data['title'] . '<br />';
            $drawItem    = false;
        }
    }

    protected function getOptionsFromFlexFormData(array $row) {
        $options         = [];
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
        $flexFormAsArray = $flexFormService->convertFlexFormContentToArray($row['pi_flexform']);


        foreach ($flexFormAsArray['options'] as $optionKey => $optionValue) {
            switch ($optionValue) {
                case '1':
                    $options[$optionKey] = true;
                    break;
                case '0':
                    $options[$optionKey] = false;
                    break;
                default:
                    $options[$optionKey] = $optionValue;
            }
        }

        return $options;
    }

}
