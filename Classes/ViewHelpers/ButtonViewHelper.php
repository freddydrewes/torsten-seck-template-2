<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ButtonViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('field', 'string', 'title', true, '');
    }

    /**
     * @return array|string
     */
    public function render() {

        $title = $this->arguments['field'];
        $tempTitle = str_replace(' ', '_', $title) . '_' . rand(0,300);

        return strtolower($tempTitle);
    }
}