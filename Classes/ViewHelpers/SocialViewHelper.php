<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class SocialViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
    }

    /**
     * @return array|string
     */
    public function render() {

        $socialMedia = [];
        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('site_package');

        $socialMedia['facebook']['link'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['facebook'];
        $socialMedia['facebook']['icon'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['facebook_icon'];
        $socialMedia['instagram']['link'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['instagram'];
        $socialMedia['instagram']['icon'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['instagram_icon'];
        $socialMedia['linkedin']['link'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['linkedin'];
        $socialMedia['linkedin']['icon'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['linkedin_icon'];
        $socialMedia['twitter']['link'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['twitter'];
        $socialMedia['twitter']['icon'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['twitter_icon'];
        $socialMedia['youtube']['link'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['youtube'];
        $socialMedia['youtube']['icon'] = $extensionConfiguration['plugin']['tx_sitepackage']['social']['youtube_icon'];

        return $socialMedia;
    }
}