<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class FooterViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
    }

    /**
     * @return array|string
     */
    public function render() {

        $footerArray = [];
        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('site_package');

        $footerArray['company_name'] = $extensionConfiguration['plugin']['tx_sitepackage']['company']['name'];
        $footerArray['company_name_one'] = $extensionConfiguration['plugin']['tx_sitepackage']['company']['name_one'];
        $footerArray['company_street'] = $extensionConfiguration['plugin']['tx_sitepackage']['company']['street_number'];
        $footerArray['company_place'] = $extensionConfiguration['plugin']['tx_sitepackage']['company']['zip_place'];
        $footerArray['logo'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['logo'];

        $footerArray['balken'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken'];
        $footerArray['menuId'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken']['menuId'];

        return $footerArray;
    }
}