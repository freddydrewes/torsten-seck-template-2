<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class BreadcrumbViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('field', 'string', 'filters', true, '');
    }

    /**
     * @return array
     */
    public function render() {

        $breadcrumb = $GLOBALS['TSFE']->rootLine;
        $newBreadcrumb = [];

        foreach ($breadcrumb as $crumb) {
            $newBreadcrumb[$crumb['uid']]['id'] = $crumb['uid'];
            $newBreadcrumb[$crumb['uid']]['title'] = $crumb['title'];
        }

        sort($newBreadcrumb);

        return $newBreadcrumb;

    }
}