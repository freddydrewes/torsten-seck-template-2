<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class TemplateViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
    }

    /**
     * @return string
     */
    public function render() {

        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('site_package');

        $primaryColor    = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['primary_color'];
        $secondaryColor  = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['secondary_color'];
        $navColor        = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['nav_color'];
        $headingColor    = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['header_color'];
        $backgroundColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['background_color'];
        $backgroundImage = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['background_image'];
        $fontDir = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_directory'];
        $navFontColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['nav_a_color'];
        $navHoverFontColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['nav_a_hover_color'];

        $bodyFont = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_body'];
        $h1Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h1'];
        $h2Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h2'];
        $h3Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h3'];
        $h4Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h4'];
        $h5Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h5'];
        $h6Font = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['font_name_for_h6'];

        $footerBalken = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken'];
        $footerFontColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken']['fontcolor'];
        $footerHoverColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken']['hoverfontcolor'];
        $footerBackgroundColor = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['footer']['balken']['backgroundcolor'];

        // Lösung mit den Schriften überlegen.
        // wenn zum Abschnitt scrollen sollen die Punkte sich ändern.

        $html = "<style>";
        if(!empty($fontDir)) {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $fontDir;
            $files = scandir($path);
            foreach ($files as $file) {
                if ( $file !='.' && $file !='..')
                {
                    $html .= "
                        @font-face {
                            font-family: '". str_replace('.ttf', '', $file) ."';
                            src: url('/" . $fontDir . $file . "');
                        }
                    ";
                }
            }
        }


        $html .= "
            body {
               font-family: '".$bodyFont."';
            }
            h1 {
               font-family: '".$h1Font."';
            }      
            h2 {
               font-family: '".$h2Font."';
            }       
            h3 {
               font-family: '".$h3Font."';
            }        
            h4 {
               font-family: '".$h4Font."';
            }        
            h5 {
               font-family: '".$h5Font."';
            }          
            h6 {
               font-family: '".$h6Font."';
            }            
        ";

        if (!empty($backgroundImage)) {
            $html .= "
                body {
                    background-image: url('/".$backgroundImage."');
                    background-position: center center;
                    background-size: cover;
                    background-repeat:no-repeat; 
                    width:100%;
                    height:100%;
                }
                ";
        }
        if (!empty($backgroundColor)) {
            $html .= " 
            body {
                background-color: ".$backgroundColor."
            }
            ";
        }

        if (!empty($primaryColor)) {
            $html .= "
                a { 
                    color: ".$primaryColor."; 
                }
                #rwd {
                    border-color: ".$primaryColor."; 
                }
                body .tabs .tab-label {
                    background-color: ".$primaryColor."; 
                }
                body .tabs .tab-label:hover {
                    background-color: ".$primaryColor."; 
                }      
                #section-nav .dot {
                    background-color: " . $primaryColor . ";
                }   
                        
            ";
        }

        if(!empty($headingColor)) {
            $html .= " 
                header#header {
                    background-color: ".$headingColor.";
                    color: " . $secondaryColor .  ";
                    font-weight: bold;
                }
            ";
        }

        if(!empty($navColor)) {
            $html .= "
                nav {
                    background-color: ".$primaryColor.";
                }
                nav a {
                    color: ".$navFontColor."
                }
                nav ul li:hover {
                    background-color: " . $navHoverFontColor . "
                }
                ul li:hover {
                    background-color: " . $navHoverFontColor . "
                }
                nav ul li.active {
                   background-color: " . $navHoverFontColor . "
                }
                ul li.active {
                   background-color: " . $navHoverFontColor . "
                }
                #footer {
                    background-color: " . $primaryColor . ";
                }
                .footer-menu a {
                    color: ".$navFontColor."
                }
                #footer {
                    background-color: " . $backgroundColor . "
                }
                .footer-menu-second a {
                   color: ".$navFontColor."
                }
            ";

        }


        if(!empty($secondaryColor)) {
            $html .= "
                 body p {
                    color: " . $secondaryColor . ";
                }
                
                .breadcrumb li a {
                    color: " . $secondaryColor . ";
                }
                
                .breadcrumb li:last-child a {
                    color: ".$primaryColor.";
                }
                
                #rwd img {
                   color: ".$primaryColor.";
                }
                
                #section-nav .dot.dot-active {
                    background-color: " . $secondaryColor . ";
                }   

            ";
        }

        if(!empty($footerBalken)) {
            $html .= ".footer-menu a:hover {
                color: ".$footerHoverColor." !important;
            }";
        }

        $html .= "</style>";

        return $html;

    }
}