<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class HeaderViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
    }

    /**
     * @return array|string
     */
    public function render() {

        $headerArray = [];
        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('site_package');

        $telephone = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['telephone_number'];


        $headerArray['logo'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['logo'];
        $headerArray['tel'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['telephone_number'];
        $headerArray['mail'] = $extensionConfiguration['plugin']['tx_sitepackage']['settings']['email_address'];


            return $headerArray;

    }
}