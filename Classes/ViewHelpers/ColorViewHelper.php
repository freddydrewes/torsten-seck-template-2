<?php

namespace SitePackage\SitePackage\ViewHelpers;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ColorViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('field', 'string', 'filters', true, '');
    }

    /**
     * @return array|string
     */
    public function render() {

        $hexColor = $this->arguments['field'];
        if(!empty($hexColor)) {
            $hex = str_replace("#", "", $hexColor);

            if(strlen($hex) == 3) {
                $r = hexdec(substr($hex,0,1).substr($hex,0,1));
                $g = hexdec(substr($hex,1,1).substr($hex,1,1));
                $b = hexdec(substr($hex,2,1).substr($hex,2,1));
            } else {
                $r = hexdec(substr($hex,0,2));
                $g = hexdec(substr($hex,2,2));
                $b = hexdec(substr($hex,4,2));
            }

            $rgb = array($r, $g, $b);

            return $rgb;
        }

        $rgb = array();

        return $rgb;


//        return array( 'red' => $r, 'green' => $g, 'blue' => $b );
    }
}