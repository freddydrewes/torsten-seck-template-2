<?php
defined('TYPO3_MODE') or die();

if (TYPO3_MODE === 'BE') {


    call_user_func(
        function ($extConfString) {

            // Add pageTS config
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/tsconfig.ts">');

        },$_EXTCONF
    );

    // Include new content elements to modWizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/Button.tsconfig">'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/Parallax.tsconfig">'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/ParallaxThreeCols.tsconfig">'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/Heading.tsconfig">'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site_package/Configuration/PageTSconfig/Social.tsconfig">'
    );

//     Register hook to show preview of tt_content element of CType="fluid_styled_slider" in page module
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['button']
        = \SitePackage\SitePackage\Hooks\ButtonPreviewRenderer::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['parallax']
        = \SitePackage\SitePackage\Hooks\ParallaxPreviewRenderer::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['parallax_three_cols']
        = \SitePackage\SitePackage\Hooks\ParallaxPreviewRenderer::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['heading']
        = \SitePackage\SitePackage\Hooks\HeadingPreviewRenderer::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['social']
        = \SitePackage\SitePackage\Hooks\SocialPreviewRenderer::class;

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

    // use same identifier as used in TSconfig for icon
    $iconRegistry->registerIcon(
    // use same identifier as used in TSconfig for icon
        'example-registration',
        \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
        // font-awesome identifier ('external-link-square')
        ['name' => 'external-link-square']
    );

    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['myPreset'] = 'EXT:site_package/Configuration/RTE/MyPreset.yaml';



}
