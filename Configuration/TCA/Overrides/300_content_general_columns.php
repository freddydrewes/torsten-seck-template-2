<?php


defined('TYPO3_MODE') || die();

$GLOBALS['TCA']['tt_content']['columns']['heading_color'] = [
    'exclude' => true,
    'label' => 'Farbe der Überschrift',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 50,
        'max' => 255
    ]
];
//
//$GLOBALS['TCA']['tt_content']['palettes']['frames']['showitem'] .= '
//    --linebreak--,
//    heading_color,
//';
