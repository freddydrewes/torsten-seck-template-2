<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {

    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    // Add the CType "heading"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:site_package/Resources/Private/Language/locallang_be.xlf:wizard.button',
            'button',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    // Add the CType "heading"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:site_package/Resources/Private/Language/locallang_be.xlf:wizard.parallax',
            'parallax',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    // Add the CType "heading"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:site_package/Resources/Private/Language/locallang_be.xlf:wizard.heading',
            'heading',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    // Add the CType "heading"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:site_package/Resources/Private/Language/locallang_be.xlf:wizard.parallax_three_cols',
            'parallax_three_cols',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    // Add the CType "heading"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:site_package/Resources/Private/Language/locallang_be.xlf:wizard.social',
            'social',
            'content-image'
        ],
        'textmedia',
        'after'
    );


    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['button'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['parallax'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['parallax_three_cols'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['heading'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['social'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['button'] = [
        'showitem'         => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform,
        ',
    ];

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['parallax'] = [
        'showitem'         => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform,
        ',
    ];

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['parallax_three_cols'] = [
        'showitem'         => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform,
        ',
    ];

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['heading'] = [
        'showitem'         => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform,
        ',
    ];

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['social'] = [
        'showitem'         => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform,
        ',
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:site_package/Configuration/FlexForms/button.xml',
        'button'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:site_package/Configuration/FlexForms/parallax.xml',
        'parallax'
    );


    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:site_package/Configuration/FlexForms/parallax_three_cols.xml',
        'parallax_three_cols'
    );


    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:site_package/Configuration/FlexForms/heading.xml',
        'heading'
    );



    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:site_package/Configuration/FlexForms/social.xml',
        'social'
    );



});
