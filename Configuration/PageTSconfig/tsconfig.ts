mod.web_layout.BackendLayouts {
    Startseite {
        title = Startseite
        icon = typo3conf/ext/site_package/Resources/Public/Icons/einfache.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Inhaltsbereich
                                colPos = 4
                            }
                        }
                    }
                }
            }
        }
    }
    Einspaltig {
        title = Einspaltig
        icon = typo3conf/ext/site_package/Resources/Public/Icons/einfache.png
        config {
            backend_layout {
                colCount = 2
                rowCount = 2
                rows {
                    1 {
                        columns {
                            1 {
                                name = Inhaltsbereich
                                colPos = 4
                            }
                        }
                    }
                    2 { å
                        columns {
                            1 {
                                name = Social Media Bereich
                                colPos = 5
                            }
                        }
                    }
                }
            }
        }
    }
    Unterseite {
        title = Unterseite
        icon = typo3conf/ext/site_package/Resources/Public/Icons/einfache.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 2
                rows {
                    2 {
                        columns {
                            1 {
                                name = Inhaltsbereich
                                colPos = 4
                            }
                        }
                    }
                }
            }
        }
    }
}